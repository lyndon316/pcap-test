#Makefile
all: pcap-test

pcap-test: main.o
	g++ -o pcap-test main.o -lpcap

main.o: main.cpp
	g++ -o main.o -c main.cpp -lpcap

clean:
	rm -f pcap-test
	rm -f main.o
