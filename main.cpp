#include <pcap.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>

void usage() {
	printf("syntax: pcap-test <interface>\n");
	printf("sample: pcap-test wlan0\n");
}

#define ETHER_ADDR_LEN 6
#define LIBNET_LIL_ENDIAN 1

struct libnet_ethernet_hdr
{
    u_int8_t  ether_dhost[ETHER_ADDR_LEN];/* destination ethernet address */
    u_int8_t  ether_shost[ETHER_ADDR_LEN];/* source ethernet address */
    u_int16_t ether_type;                 /* protocol */
};

/*
 *  IPv4 header
 *  Internet Protocol, version 4
 *  Static header size: 20 bytes
 */
struct libnet_ipv4_hdr
{
#if (LIBNET_LIL_ENDIAN)
    u_int8_t ip_hl:4,      /* header length */
           ip_v:4;         /* version */
#endif
#if (LIBNET_BIG_ENDIAN)
    u_int8_t ip_v:4,       /* version */
           ip_hl:4;        /* header length */
#endif
    u_int8_t ip_tos;       /* type of service */
#ifndef IPTOS_LOWDELAY
#define IPTOS_LOWDELAY      0x10
#endif
#ifndef IPTOS_THROUGHPUT
#define IPTOS_THROUGHPUT    0x08
#endif
#ifndef IPTOS_RELIABILITY
#define IPTOS_RELIABILITY   0x04
#endif
#ifndef IPTOS_LOWCOST
#define IPTOS_LOWCOST       0x02
#endif
    u_int16_t ip_len;         /* total length */
    u_int16_t ip_id;          /* identification */
    u_int16_t ip_off;
#ifndef IP_RF
#define IP_RF 0x8000        /* reserved fragment flag */
#endif
#ifndef IP_DF
#define IP_DF 0x4000        /* dont fragment flag */
#endif
#ifndef IP_MF
#define IP_MF 0x2000        /* more fragments flag */
#endif 
#ifndef IP_OFFMASK
#define IP_OFFMASK 0x1fff   /* mask for fragmenting bits */
#endif
    u_int8_t ip_ttl;          /* time to live */
    u_int8_t ip_p;            /* protocol */
    u_int16_t ip_sum;         /* checksum */
    struct in_addr ip_src, ip_dst; /* source and dest address */
};

/*
 *  TCP header
 *  Transmission Control Protocol
 *  Static header size: 20 bytes
 */
struct libnet_tcp_hdr
{
    u_int16_t th_sport;       /* source port */
    u_int16_t th_dport;       /* destination port */
    u_int32_t th_seq;          /* sequence number */
    u_int32_t th_ack;          /* acknowledgement number */
#if (LIBNET_LIL_ENDIAN)
    u_int8_t th_x2:4,         /* (unused) */
           th_off:4;        /* data offset */
#endif
#if (LIBNET_BIG_ENDIAN)
    u_int8_t th_off:4,        /* data offset */
           th_x2:4;         /* (unused) */
#endif
    u_int8_t  th_flags;       /* control flags */
#ifndef TH_FIN
#define TH_FIN    0x01      /* finished send data */
#endif
#ifndef TH_SYN
#define TH_SYN    0x02      /* synchronize sequence numbers */
#endif
#ifndef TH_RST
#define TH_RST    0x04      /* reset the connection */
#endif
#ifndef TH_PUSH
#define TH_PUSH   0x08      /* push data to the app layer */
#endif
#ifndef TH_ACK
#define TH_ACK    0x10      /* acknowledge */
#endif
#ifndef TH_URG
#define TH_URG    0x20      /* urgent! */
#endif
#ifndef TH_ECE
#define TH_ECE    0x40
#endif
#ifndef TH_CWR   
#define TH_CWR    0x80
#endif
    u_int16_t th_win;         /* window */
    u_int16_t th_sum;         /* checksum */
    u_int16_t th_urp;         /* urgent pointer */
};

struct data_packet
{
	u_char asdf[10];
};

typedef struct {
	char* dev_;
} Param;

Param param = {
	.dev_ = NULL
};

bool parse(Param* param, int argc, char* argv[]) {
	if (argc != 2) {
		usage();
		return false;
	}
	param->dev_ = argv[1];
	return true;
}

void printMac(u_int8_t* mac_arr)
{
	for(int i = 0; i < ETHER_ADDR_LEN - 1; i++)
	{
		printf("%02x::", mac_arr[i]);
	}
	printf("%02x\n", mac_arr[ETHER_ADDR_LEN - 1]);
}

void printxxd(u_char* arr, int n)
{
	for(int i = 0; i < n; i++)
	{
		printf("%02x ", arr[i]);	
	}
}

int main(int argc, char* argv[]) {
	if (!parse(&param, argc, argv))
		return -1;

	char errbuf[PCAP_ERRBUF_SIZE];
	pcap_t* pcap = pcap_open_live(param.dev_, BUFSIZ, 1, 1000, errbuf);
	if (pcap == NULL) {
		fprintf(stderr, "pcap_open_live(%s) return null - %s\n", param.dev_, errbuf);
		return -1;
	}

	struct pcap_pkthdr* header;
	const u_char* packet = (u_char*)malloc(BUFSIZ);
	if(packet == NULL)
	{
		fprintf(stderr, "malloc return null\n");
		return -1;
	}

	while (true) {
		int res = pcap_next_ex(pcap, &header, &packet);
		if (res == 0) continue;
		if (res == PCAP_ERROR || res == PCAP_ERROR_BREAK) {
			printf("pcap_next_ex return %d(%s)\n", res, pcap_geterr(pcap));
			break;
		}

		void* ethernet_header = (u_char*)packet;
		void* ip_header = (u_char*)packet + sizeof(libnet_ethernet_hdr);
		void* tcp_header = (u_char*)ip_header + ((libnet_ipv4_hdr*)ip_header)->ip_hl * 4;
		void* data = (u_char*)tcp_header + ((libnet_tcp_hdr*)tcp_header)->th_off * 4;

		if(((libnet_ipv4_hdr*)ip_header)->ip_v != 4 || ((libnet_ipv4_hdr*)ip_header)->ip_p != 6)
			continue;

		printf("===========================================\n");
		printf("%u bytes captured\n", header->caplen);


		int dsize = header->caplen - sizeof(libnet_ethernet_hdr) - ((libnet_ipv4_hdr*)ip_header)->ip_hl * 4 - ((libnet_tcp_hdr*)tcp_header)->th_off * 4;
		if(dsize < 0)
		{
			fprintf(stderr, "packet parsing error : header size is larger than packet\n");
			continue;
		}

		printf("Ethernet header src mac - ");
		printMac(((libnet_ethernet_hdr*)ethernet_header)->ether_shost);
		printf("Ethernet header dst mac - ");
		printMac(((libnet_ethernet_hdr*)ethernet_header)->ether_dhost);


		printf("IP header src IP - %s\n", inet_ntoa(((libnet_ipv4_hdr*)ip_header)->ip_src));
		printf("IP header dst IP - %s\n", inet_ntoa(((libnet_ipv4_hdr*)ip_header)->ip_dst));
		
		printf("TCP header src Port - %d\n", ntohs(((libnet_tcp_hdr*)tcp_header)->th_sport));
		printf("TCP header dst Port - %d\n", ntohs(((libnet_tcp_hdr*)tcp_header)->th_dport));
		//printf("off - %d\n", ((libnet_tcp_hdr*)tcp_header)->th_off);
		printf("Data - ");
		printxxd(((data_packet*)data)->asdf, dsize>=10 ? 10 : dsize);
		printf("\n");
	}
	free((u_char*)packet);
	pcap_close(pcap);
}
